package com.applaudostudios.mediaplayer.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.applaudostudios.mediaplayer.R;
import com.applaudostudios.mediaplayer.adapters.RadioAdapter;
import com.applaudostudios.mediaplayer.model.Radio;
import com.applaudostudios.mediaplayer.services.PlayRadioService;
import com.applaudostudios.mediaplayer.utils.NotificationHandle;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        RadioAdapter.OnItemRadioClick, PlayRadioService.OnCallBackClient {

    private final String EXT_RADIO_NAME = "EXT_RADIO_NAME";
    private PlayRadioService mPlayRadioService;
    private FloatingActionButton mFabPlayOrStop;
    private FloatingActionButton mFabMute;
    private RecyclerView mRcvRadios;
    private TextView mTxvRadioName;
    private boolean mIsBound = false;
    private String mRadioName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView(savedInstanceState);
    }

    /**
     * init the views od this activity
     */
    public void initView(Bundle instance) {
        mFabPlayOrStop = findViewById(R.id.fabPlayMusic);
        FloatingActionButton fabDetail = findViewById(R.id.fabInfo);
        mFabMute = findViewById(R.id.fabMute);
        mTxvRadioName = findViewById(R.id.txvNameRadio);
        RadioAdapter radioAdapter = new RadioAdapter(this);
        mRcvRadios = findViewById(R.id.rcvRadios);
        mRcvRadios.setFocusable(false);
        mRcvRadios.setNestedScrollingEnabled(false);
        mRcvRadios.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        DividerItemDecoration dividerItemDecoration =
                new DividerItemDecoration(mRcvRadios.getContext(), DividerItemDecoration.VERTICAL);
        mRcvRadios.addItemDecoration(dividerItemDecoration);
        mRcvRadios.setAdapter(radioAdapter);
        radioAdapter.setData(Radio.getRadios());
        if (getIntent() != null && getIntent().getAction() != null) {
            if (getIntent().getAction().equals(PlayRadioService.MAIN_ACTION)) {
                mRadioName = getIntent().getStringExtra(NotificationHandle.EXT_RADIO_NAME);
                mTxvRadioName.setText(mRadioName);
            }
        }
        if (instance != null) {
            mRadioName = instance.getString(EXT_RADIO_NAME);
            mTxvRadioName.setText(mRadioName);
        }
        mFabPlayOrStop.setOnClickListener(this);
        fabDetail.setOnClickListener(this);
        mFabMute.setOnClickListener(this);
    }

    /**
     * this function init the service
     *
     * @param action action to send to service
     * @param bundle data to send to service
     */
    public void initServices(String action, Bundle bundle) {
        startService(PlayRadioService.getIntent(this, action,
                bundle));
    }

    @Override
    protected void onStart() {
        super.onStart();
        doBindService();
    }


    /**
     * Bind this activity with PlayRadioService
     */
    void doBindService() {
        bindService(new Intent(this, PlayRadioService.class),
                mConnection,
                Context.BIND_AUTO_CREATE);
        mIsBound = true;
    }

    /**
     * UnBind this activity with PlayRadioService
     */
    void doUnbindService() {
        if (mIsBound) {
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        doUnbindService();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabPlayMusic:
                if (!mRadioName.equals("")) {
                    if (mPlayRadioService != null) {
                        if (mPlayRadioService.getMPlay() == PlayRadioService.MEDIA_PLAYER_PLAY) {
                            initServices(PlayRadioService.STOP_ACTION, null);
                        } else if (mPlayRadioService.getMPlay() == PlayRadioService.MEDIA_PLAYER_PAUSE) {
                            initServices(PlayRadioService.PLAY_ACTION, null);
                            break;
                        } else {
                            Snackbar.make(mRcvRadios, R.string.message_wait_please, Snackbar.LENGTH_SHORT).show();
                        }
                    } else {
                        initServices("",null);
                        Snackbar.make(mRcvRadios, R.string.message_wait_please, Snackbar.LENGTH_SHORT).show();
                    }

                } else {
                    Snackbar.make(mRcvRadios, R.string.message_selected_radio, Snackbar.LENGTH_SHORT).show();
                }

                break;
            case R.id.fabInfo:
                startActivity(DetailActivity.getIntent(this, mRadioName));
                break;
            case R.id.fabMute:
                if (mPlayRadioService != null) {
                    if (mPlayRadioService.getMPlay() == PlayRadioService.MEDIA_PLAYER_PLAY) {
                        if (mPlayRadioService.getStateMute()) {
                            initServices(PlayRadioService.UNMUTE_ACTION, null);
                        } else {
                            initServices(PlayRadioService.MUTE_ACTION, null);
                        }
                    } else {
                        Snackbar.make(mRcvRadios, getString(R.string.play_radio_please), Snackbar.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPlayRadioService = ((PlayRadioService.LocalBinder) service).getService();
            if (mPlayRadioService != null) {
                switch (mPlayRadioService.getMPlay()) {
                    case PlayRadioService.MEDIA_PLAYER_PLAY:
                        changeDrawableButton(mFabPlayOrStop, R.drawable.ic_pause_black_24dp, null);
                        break;
                    case PlayRadioService.MEDIA_PLAYER_PAUSE:
                        changeDrawableButton(mFabPlayOrStop, R.drawable.ic_play_arrow_black_24dp, null);
                        break;
                    case PlayRadioService.MEDIA_PLAYER_PREPARE:
                        changeDrawableButton(mFabPlayOrStop, R.drawable.ic_play_arrow_black_24dp, null);
                        break;
                }
                if (mPlayRadioService.getStateMute()) {
                    changeDrawableButton(mFabMute, R.drawable.ic_volume_up_black_24dp, null);
                } else {
                    changeDrawableButton(mFabMute, R.drawable.ic_volume_off_black_24dp, null);
                }
                mPlayRadioService.registerClient(MainActivity.this);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPlayRadioService = null;
        }
    };


    /**
     * Change the drawable to FloatingActionButton and Shows a SnackBar with message
     *
     * @param view       of type FloatingActionButton
     * @param idDrawable id with the drawable
     * @param message    String with the message
     */
    public void changeDrawableButton(FloatingActionButton view, int idDrawable, Integer message) {
        view.setImageDrawable(getResources().getDrawable(idDrawable));
        if (message != null) {
            Snackbar.make(mRcvRadios, message, Snackbar.LENGTH_SHORT).show();
        }
    }


    /**
     * This function is executed when is clicked a item of the RecyclerView
     * and have as parameter the name of the radio.
     *
     * @param radio parameter of type String
     */
    @Override
    public void onClickRadio(String radio) {
        mRadioName = radio;
        mTxvRadioName.setText(radio);
        Bundle b = new Bundle();
        b.putString(PlayRadioService.EXT_URL_RADIO, radio);
        initServices(PlayRadioService.START_PLAY_ACTION, b);
        Snackbar.make(mRcvRadios, R.string.message_wait_please, Snackbar.LENGTH_INDEFINITE).show();
    }

    /**
     * This function change the drawable to the Button play
     * and send a message.
     *
     * @param status true for playing and false for pause
     */
    @Override
    public void isPlay(boolean status) {
        if (status) {
            changeDrawableButton(mFabPlayOrStop, R.drawable.ic_pause_black_24dp, R.string.message_play_radio);
        } else {
            changeDrawableButton(mFabPlayOrStop, R.drawable.ic_play_arrow_black_24dp, R.string.message_pause_radio);
        }
    }

    /**
     * This function change the drawable to the Button mute
     * and send a message
     *
     * @param status true for mute and false for not mute
     */
    @Override
    public void isMute(boolean status) {
        if (status) {
            changeDrawableButton(mFabMute, R.drawable.ic_volume_up_black_24dp, R.string.message_volume_off);
        } else {
            changeDrawableButton(mFabMute, R.drawable.ic_volume_off_black_24dp, R.string.message_volume_up);
        }
    }

    /**
     * set variable mRadioName = "" and set drawable play to button PlayOrStop
     */
    @Override
    public void notificationClose() {
        doUnbindService();
        mRadioName = "";
        mTxvRadioName.setText(mRadioName);
        changeDrawableButton(mFabPlayOrStop, R.drawable.ic_play_arrow_black_24dp, null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(EXT_RADIO_NAME, mRadioName);
        super.onSaveInstanceState(outState);
    }
}
