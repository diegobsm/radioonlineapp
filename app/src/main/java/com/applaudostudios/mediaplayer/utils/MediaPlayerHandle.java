package com.applaudostudios.mediaplayer.utils;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;

import java.io.IOException;

public class MediaPlayerHandle {

    private static MediaPlayer mMediaPlayer;

    private MediaPlayerHandle() {
        mMediaPlayer = new MediaPlayer();
    }

    /**
     *
     * This function starts the instances of MediaPLayer and MediaPlayerHandle
     *
     * @return object of mediaPlayerHandle
     */
    public static MediaPlayerHandle getInstance() {
        mMediaPlayer = new MediaPlayer();
        return new MediaPlayerHandle();
    }

    /**
     *
     * This function returns the object MediaPLayer
     *
     * @return object MediaPLayer
     */
    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    /**
     *
     * @param url
     */
    public void loadDataSourceAndPrepare(String url) {
        mMediaPlayer.reset();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            mMediaPlayer.setAudioAttributes(audioAttributes);
        } else {
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }
        try {
            mMediaPlayer.setDataSource(url);
            mMediaPlayer.prepareAsync();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *this function low and hing de volume of stream
     *
     * @param status is used for make the condition for evaluate
     */
    public void actionMute(boolean status){
        if (status) {
            mMediaPlayer.setVolume(0, 0);
        }else {
            mMediaPlayer.setVolume(1,1);
        }
    }

    /**
     *This function, start he playing of the stream
     */
    public void startRadio() {
        mMediaPlayer.start();
    }

    /**
     *this function, pauses the playing of the stream.
     */
    public void pauseRadio() {
        mMediaPlayer.pause();
    }

    /**
     *this function, is called when the object MediaPlayer already not used.
     */
    public void close() {
        mMediaPlayer.release();
        mMediaPlayer = null;
    }

}
