package com.applaudostudios.mediaplayer.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.applaudostudios.mediaplayer.utils.MediaPlayerHandle;
import com.applaudostudios.mediaplayer.utils.NotificationHandle;

public class PlayRadioService extends Service implements MediaPlayer.OnPreparedListener {

    private MediaPlayerHandle mMediaPlayerHandle;
    private String urlRadio = "";
    private Binder mBinder = new LocalBinder();
    private OnCallBackClient mListener;

    public static final String START_PLAY_ACTION = "com.applaudostudios.mediaplayer.start.play.action";
    public static final String PLAY_ACTION = "com.applaudostudios.mediaplayer.play.action";
    public static final String STOP_ACTION = "com.applaudostudios.mediaplayer.stop.action";
    public static final String MAIN_ACTION = "com.applaudostudios.mediaplayer.open.main";
    public static final String MUTE_ACTION = "com.applaudostudios.mediaplayer.mute.action";
    public static final String UNMUTE_ACTION = "com.applaudostudios.mediaplayer.unmute.action";
    public static final String EXT_URL_RADIO = "com.applaudostudios.mediaplayer.name.radio";
    public static final String DELETE_ACTION = "com.applaudostudios.mediaplayer.delete.notifiction";
    public static final String CLOSE_ACTION = "com.applaudostudios.mediaplayer.close.notifiction";

    public static final String CHANNEL_LISTEN = "listen";
    public static final String EXTRAS = "EXTRAS";
    public static final int MEDIA_PLAYER_PREPARE = 0;
    public static final int MEDIA_PLAYER_PLAY = 1;
    public static final int MEDIA_PLAYER_PAUSE = 2;
    public static int FOREGROUND_SERVICE = 101;

    private int mPlay = 0;
    private boolean isMute = false;
    private int actionPlay = 1;
    private int actionMute = 3;


    /**
     *
     * This function creates a intent with data gives
     *
     * @param context for creates intent
     * @param action action a execute
     * @param bundle data sending
     * @return intent
     */
    public static Intent getIntent(Context context, String action, Bundle bundle) {
        Intent intent = new Intent(context, PlayRadioService.class);
        if (!action.equals("")) {
            intent.setAction(action);
            intent.putExtra(EXTRAS, bundle);
        }
        return intent;
    }

    public PlayRadioService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mMediaPlayerHandle = MediaPlayerHandle.getInstance();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            switch (intent.getAction()) {
                case START_PLAY_ACTION:
                    mPlay = MEDIA_PLAYER_PREPARE;
                    urlRadio = intent.getBundleExtra(EXTRAS).getString(EXT_URL_RADIO);
                    prepareMediaPlayer(urlRadio);
                    break;
                case PLAY_ACTION:
                    mMediaPlayerHandle.startRadio();
                    mPlay = MEDIA_PLAYER_PLAY;
                    actionPlay = 1;
                    showNotificationAndSendMessage(urlRadio, PLAY_ACTION, actionPlay, actionMute);
                    break;
                case STOP_ACTION:
                    mMediaPlayerHandle.pauseRadio();
                    mPlay = MEDIA_PLAYER_PAUSE;
                    actionPlay = 2;
                    showNotificationAndSendMessage(urlRadio, STOP_ACTION, actionPlay, actionMute);
                    break;
                case DELETE_ACTION:
                    stopForeground(true);
                    stopSelf();
                    break;
                case MUTE_ACTION:
                    mMediaPlayerHandle.actionMute(true);
                    actionMute = 4;
                    showNotificationAndSendMessage(urlRadio, MUTE_ACTION, actionPlay, actionMute);
                    isMute = true;
                    break;
                case UNMUTE_ACTION:
                    mMediaPlayerHandle.actionMute(false);
                    actionMute = 3;
                    showNotificationAndSendMessage(urlRadio, UNMUTE_ACTION, actionPlay, actionMute);
                    isMute = false;
                    break;
                case CLOSE_ACTION:
                    setMessage(CLOSE_ACTION);
                    stopForeground(true);
                    stopSelf();
                    break;
            }
        }
        return START_STICKY;
    }


    /**
     * This function set dataSource to MediaPlayer and prepare the MediaPlayer
     *
     * @param url for set to MediaPlayer.setDataSource(url)
     */
    private void prepareMediaPlayer(String url) {
        mMediaPlayerHandle.getMediaPlayer().setOnPreparedListener(this);
        mMediaPlayerHandle.loadDataSourceAndPrepare(url);
    }

    /**
     * this function start notification with method startForeground()
     * and notify to MainActivity when is execute a action in startCommand
     *
     * @param name radio
     * @param actionMedia stateActionPlay and stateActionMute
     * @param action actionMedia play or stop
     */
    public void showNotificationAndSendMessage(String name, String action, int... actionMedia) {
        startForeground(FOREGROUND_SERVICE, NotificationHandle.showNotificationCustomRadio(this, name, actionMedia));
        setMessage(action);
    }

    /**
     * This function notify to the Activity.
     * depending of the parameters action
     *
     * @param action for make condition
     */
    public void setMessage(String action) {
        switch (action) {
            case PLAY_ACTION:
                mListener.isPlay(true);
                break;
            case STOP_ACTION:
                mListener.isPlay(false);
                break;
            case MUTE_ACTION:
                mListener.isMute(true);
                break;
            case UNMUTE_ACTION:
                mListener.isMute(false);
                break;
            case CLOSE_ACTION:
                mListener.notificationClose();
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMediaPlayerHandle.close();
    }

    /**
     * this function return the state the mPLay
     *
     * @return true or false if es played or  paused
     */
    public int getMPlay() {
        return mPlay;
    }


    /**
     * This function close the contract between MainActivity and PlayRadioService
     * Where the Service will notify to MainActivity
     *
     * @param callBackClient object that implement the interface
     */
    public void registerClient(OnCallBackClient callBackClient) {
        mListener = callBackClient;
    }

    /**
     * This function return is radio is mute or not mute
     *
     * @return is mute or not mute
     */
    public boolean getStateMute() {
        return isMute;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mp.start();
        mPlay = MEDIA_PLAYER_PLAY;
        actionPlay = 1;
        showNotificationAndSendMessage(urlRadio, PLAY_ACTION, actionPlay, actionMute);
    }

    public class LocalBinder extends Binder {
        public PlayRadioService getService() {
            return PlayRadioService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /**
     * this interface define the callbacks that are called when is execute the action
     * play or pause with de method isPlay() and mute or not mute with the method isMute()
     */
    public interface OnCallBackClient {
        /**
         * this is get a parameter of type boolean
         *
         * @param status true for playing and false for pause
         */
        void isPlay(boolean status);

        /**
         * this function get a parameter of type boolean
         *
         * @param status true for mute and false for not mute
         */
        void isMute(boolean status);

        /**
         * this function notify when notification is close
         */
        void notificationClose();
    }
}
